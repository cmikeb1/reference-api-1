package org.hspconsortium.platform.fhir;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.context.RuntimeResourceDefinition;
import ca.uhn.fhir.context.RuntimeSearchParam;
import ca.uhn.fhir.jpa.dao.IFhirSystemDao;
import ca.uhn.fhir.model.dstu2.resource.Bundle;
import ca.uhn.fhir.model.dstu2.resource.Conformance;
import ca.uhn.fhir.model.dstu2.resource.Conformance.Rest;
import ca.uhn.fhir.model.dstu2.resource.Conformance.RestResource;
import ca.uhn.fhir.model.dstu2.resource.Conformance.RestResourceSearchParam;
import ca.uhn.fhir.model.dstu2.valueset.ResourceTypeEnum;
import ca.uhn.fhir.model.dstu2.valueset.SearchParamTypeEnum;
import ca.uhn.fhir.model.primitive.BoundCodeDt;
import ca.uhn.fhir.model.primitive.DecimalDt;
import ca.uhn.fhir.rest.server.RestfulServer;
import ca.uhn.fhir.rest.server.provider.dstu2.ServerConformanceProvider;
import ca.uhn.fhir.util.ExtensionConstants;

/**
 * This is a temporary replacement for ca.uhn.fhir.jpa.provider.JpaConformanceProviderDstu2.  The changes allow the
 * class to be autowired with spring.
 *
 * I created a pull request with HAPI to include this is v1.3, so this temporary class should no longer be needed after
 * upgrading to that 1.3.
 *
 * https://github.com/jamesagnew/hapi-fhir/pull/254
 */
public class TempJpaConformanceProviderDstu2 extends ServerConformanceProvider {

    private String myImplementationDescription;
    private IFhirSystemDao<Bundle> mySystemDao;
    private volatile Conformance myCachedValue;
    private RestfulServer myRestfulServer;

    public TempJpaConformanceProviderDstu2(RestfulServer theRestfulServer, IFhirSystemDao<Bundle> theSystemDao) {
        super(theRestfulServer);
        myRestfulServer = theRestfulServer;
        mySystemDao = theSystemDao;
        super.setCache(false);
    }

    public TempJpaConformanceProviderDstu2(){
        super();
        super.setCache(false);
    }

    @Override
    public Conformance getServerConformance(HttpServletRequest theRequest) {
        Conformance retVal = myCachedValue;

        Map<String, Long> counts = mySystemDao.getResourceCounts();

        FhirContext ctx = myRestfulServer.getFhirContext();

        retVal = super.getServerConformance(theRequest);
        for (Rest nextRest : retVal.getRest()) {
            for (RestResource nextResource : nextRest.getResource()) {

                // Add resource counts
                Long count = counts.get(nextResource.getTypeElement().getValueAsString());
                if (count != null) {
                    nextResource.addUndeclaredExtension(false, ExtensionConstants.CONF_RESOURCE_COUNT, new DecimalDt(count));
                }

                // Add chained params
                for (RestResourceSearchParam nextParam : nextResource.getSearchParam()) {
                    if (nextParam.getTypeElement().getValueAsEnum() == SearchParamTypeEnum.REFERENCE) {
                        List<BoundCodeDt<ResourceTypeEnum>> targets = nextParam.getTarget();
                        for (BoundCodeDt<ResourceTypeEnum> next : targets) {
                            RuntimeResourceDefinition def = ctx.getResourceDefinition(next.getValue());
                            for (RuntimeSearchParam nextChainedParam : def.getSearchParams()) {
                                nextParam.addChain(nextChainedParam.getName());
                            }
                        }
                    }
                }

            }
        }

        retVal.getImplementation().setDescription(myImplementationDescription);
        myCachedValue = retVal;
        return retVal;
    }

    public void setMySystemDao(IFhirSystemDao<Bundle> mySystemDao) {
        this.mySystemDao = mySystemDao;
    }

    @Override
    public void setRestfulServer(RestfulServer theRestfulServer) {
        this.myRestfulServer = theRestfulServer;
        super.setRestfulServer(theRestfulServer);
    }

    public void setImplementationDescription(String theImplDesc) {
        myImplementationDescription = theImplDesc;
    }

}