/*
 * #%L
 * Healthcare Services Consortium Platform FHIR Server
 * %%
 * Copyright (C) 2014 - 2015 Healthcare Services Platform Consortium
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

package org.hspconsortium.platform.fhir;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.context.FhirVersionEnum;
import ca.uhn.fhir.jpa.dao.IFhirSystemDao;
import ca.uhn.fhir.jpa.provider.JpaConformanceProviderDstu1;
import ca.uhn.fhir.jpa.provider.JpaConformanceProviderDstu2;
import ca.uhn.fhir.jpa.provider.JpaSystemProviderDstu1;
import ca.uhn.fhir.jpa.provider.JpaSystemProviderDstu2;
import ca.uhn.fhir.model.api.IResource;
import ca.uhn.fhir.model.dstu2.resource.Bundle;
import ca.uhn.fhir.narrative.DefaultThymeleafNarrativeGenerator;
import ca.uhn.fhir.rest.server.*;
import ca.uhn.fhir.rest.server.interceptor.IServerInterceptor;
import ca.uhn.fhir.rest.server.provider.dstu2.ServerConformanceProvider;
import com.sun.corba.se.spi.activation.Server;
import org.hspconsortium.platform.service.repository.MetadataRepository;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletException;
import java.util.List;

public class FHIRService extends RestfulServer {

    private static final long serialVersionUID = 1L;

    private WebApplicationContext myAppCtx;

    @SuppressWarnings("unchecked")
    @Override
    protected void initialize() throws ServletException {
        super.initialize();

        /*
           * We want to support FHIR DSTU2 format. This means that the server
           * will use the DSTU2 bundle format and other DSTU2 encoding changes.
           *
           * If you want to use DSTU1 instead, change the following line, and change the 2 occurrences of dstu2 in web.xml to dstu1
           */
        FhirVersionEnum fhirVersion = FhirVersionEnum.DSTU2;
        setFhirContext(new FhirContext(fhirVersion));

        // Get the spring context from the web container (it's declared in web.xml)
        myAppCtx = WebApplicationContextUtils.getWebApplicationContext(this.getServletContext());

        /*
           * The hapi-fhir-server-resourceproviders-dev.xml file is a spring configuration
           * file which is automatically generated as a part of hapi-fhir-jpaserver-base and
           * contains bean definitions for a resource provider for each resource type
           */
        String resourceProviderBeanName = "myResourceProvidersDstu" + (fhirVersion == FhirVersionEnum.DSTU1 ? "1" : "2");
        List<IResourceProvider> beans = myAppCtx.getBean(resourceProviderBeanName, List.class);
        setResourceProviders(beans);

        /*
           * The system provider implements non-resource-type methods, such as
           * transaction, and global history.
           */
        Object systemProvider;
        if (fhirVersion == FhirVersionEnum.DSTU1) {
            systemProvider = myAppCtx.getBean("mySystemProviderDstu1");
        } else {
            systemProvider = myAppCtx.getBean("mySystemProviderDstu2");
        }
        setPlainProviders(systemProvider);

        /**
         * The conformance provider implementation is wired in spring.
         */
        ServerConformanceProvider confProvider = myAppCtx.getBean("myConformanceProviderDstu2", ServerConformanceProvider.class);
        setServerConformanceProvider(confProvider);


        /*
           * Enable ETag Support (this is already the default)
           */
        setETagSupport(ETagSupportEnum.ENABLED);

        /*
           * This server tries to dynamically generate narratives
           */
        FhirContext ctx = getFhirContext();
        ctx.setNarrativeGenerator(new DefaultThymeleafNarrativeGenerator());

        /*
           * This tells the server to use "browser friendly" MIME types if it
           * detects that the request is coming from a browser, in the hopes that the
           * browser won't just treat the content as a binary payload and try
           * to download it (which is what generally happens if you load a
           * FHIR URL in a browser).
           *
           * This means that the server isn't technically complying with the
           * FHIR specification for direct browser requests, but this mode
           * is very helpful for testing and troubleshooting since it means
           * you can look at FHIR URLs directly in a browser.
           */
        setUseBrowserFriendlyContentTypes(true);

        /*
           * Default to XML and pretty printing
           */
        setDefaultPrettyPrint(true);
        setDefaultResponseEncoding(EncodingEnum.JSON);

        /*
           * This is a simple paging strategy that keeps the last 10 searches in memory
           */
        setPagingProvider(new FifoMemoryPagingProvider(10));

        /*
           * Load interceptors for the server from Spring (these are defined in fhir-service-config.xml
           */
        List<IServerInterceptor> interceptorBeans = myAppCtx.getBean("myServerInterceptors", List.class);
        for (IServerInterceptor interceptor : interceptorBeans) {
            this.registerInterceptor(interceptor);
        }

    }
}
